import React, {Component} from "react";
import {getDatabase, ref, child, onValue, get} from "firebase/database"
import {initializeApp} from "firebase/app";
import {getAnalytics} from "firebase/analytics";
import WeatherTableTd from "./WeatherTabeTd"
import axios from "axios";

const firebaseConfig = {
    apiKey: "AIzaSyDXToVvYRgCzFnO9Je8M2JveVJUfOpKZVA",
    authDomain: "testdb1-532be.firebaseapp.com",
    databaseURL: "https://testdb1-532be-default-rtdb.firebaseio.com",
    projectId: "testdb1-532be",
    storageBucket: "testdb1-532be.appspot.com",
    messagingSenderId: "823141934052",
    appId: "1:823141934052:web:d1b10b42650e03d737ddf1",
    measurementId: "G-GPCZBJZ7XF"
};

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const db = getDatabase();


class Home extends Component {


    constructor(props) {
        super(props);
        this.state = {
            weatherData: [],
            description: "",
            temp: "",
            humidity: "",
            reqFrequency: 5,
            updateFrequency: 10
        };
        this.getDataRealtime = this.getDataRealtime.bind(this);
        this.getCurrentWeatherDescription = this.getCurrentWeatherDescription.bind(this);
        this.getCurrentTemperature = this.getCurrentTemperature.bind(this);
        this.getCurrentHumidity = this.getCurrentHumidity.bind(this);
        this.changeRequestFrequency = this.changeRequestFrequency.bind(this);
        this.changeUpdateFrequency = this.changeUpdateFrequency.bind(this);
        this.onChangeRequest = this.onChangeRequest.bind(this);
        this.onChangeUpdateFrequency = this.onChangeUpdateFrequency.bind(this);

    }


    componentDidMount() {
        this.getCurrentWeatherDescription();
        this.getCurrentTemperature();
        this.getCurrentHumidity();
        this.getDataRealtime();

    }

    getCurrentWeatherDescription() {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        axios.get("http://localhost:8080/weather/getCurrentWeatherDescription", axiosConfig)
            .then((res) => {
                this.setState({
                    description: res.data
                })
            }).catch((err) => {
            console.log(err)
        });

    }


    getCurrentTemperature() {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        axios.get("http://localhost:8080/weather/getCurrentTemperature", axiosConfig)
            .then((res) => {
                this.setState({
                    temp: res.data
                })
            }).catch((err) => {
            console.log(err)
        });
    }

    getCurrentHumidity() {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        axios.get("http://localhost:8080/weather/getCurrentHumidity", axiosConfig)
            .then((res) => {
                this.setState({
                    humidity: res.data
                })
            }).catch((err) => {
            console.log(err)
        });
    }

    getDataRealtime() {
        const dbRef = ref(db, "weatherInfo");

        onValue(dbRef, (snapshot) => {
            var weatherList = [];
            snapshot.forEach(childSnapshot => {
                weatherList.push(childSnapshot.val());
            });
            console.log(weatherList);
            this.setState({weatherData: weatherList});
            console.log(this.state.weatherData);
            return weatherList;
        })
    }

    onChangeRequest(e) {
        this.setState({
            reqFrequency: e.target.value
        });
    }

    changeRequestFrequency() {
        let frequency = this.state.reqFrequency;

        let axiosConfig = {
            headers: {
                'Content-Type': 'application/text;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        axios.post("http://localhost:8080/weather/mapFrequency",
            frequency
            ,
            axiosConfig
        )
            .then((res) => {
                //
            }).catch((err) => {
            console.log(err)
        });
    }

    onChangeUpdateFrequency(e) {
        this.setState({
            updateFrequency: e.target.value
        });
    }

    changeUpdateFrequency(e) {
        let frequency = this.state.updateFrequency;

        let axiosConfig = {
            headers: {
                'Content-Type': 'application/text;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        axios.post("http://localhost:8080/weather/dbUpdateFrequency",
            frequency
            ,
            axiosConfig
        )
            .then((res) => {
                //
            }).catch((err) => {
            console.log(err)
        });
    }


    render() {
        let i = 0;
        return (
            <>
                <div className="container">
                    <br/>
                    <br/>

                    <form className="row">
                        <h1>Open Weather Map Real Time Data</h1>
                        <hr/>
                        <div className="form-group col-md-4">
                            <label htmlFor="openWeather">Open Weather Map request Frequency</label>
                            <input type="number" min={0} className="form-control" id="openWeather"
                                   aria-describedby="emailHelp" placeholder="Enter Frequency in Seconds"
                                   onChange={this.onChangeRequest}
                            />
                        </div>
                        <div className="col-md-2">
                            <br/>
                            <button type="button" className="btn btn-success"
                                    onClick={this.changeRequestFrequency}
                            >Request Frequency
                            </button>
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="realtimeDb">Realtime database Update Frequency</label>
                            <input type="number" min={0} className="form-control" id="realtimeDb"
                                   placeholder="Enter Frequency in Seconds"
                                   onChange={this.onChangeUpdateFrequency}
                            />
                        </div>
                        <div className="col-md-2">
                            <br/>
                            <button type="button" className="btn btn-success"
                                    onClick={ this.changeUpdateFrequency}
                            > Update Frequency
                            </button>
                        </div>
                    </form>

                    <hr/>
                    <br/>
                    <div className="container">
                        <div className="row">
                            <div className="card col-md-4">
                                <h5 className="card-title">Weather Info at Page Loading</h5>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Weather Description : {this.state.description}</li>
                                    <li className="list-group-item">Temperature : {this.state.temp}</li>
                                    <li className="list-group-item">Humidity : {this.state.humidity}</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <hr/>
                    <br/>
                    <h5 className="card-title">Weather Info History</h5>

                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Description</th>
                            <th scope="col">Temperature</th>
                            <th scope="col">Humidity</th>
                            <th scope="col">1 hour average temperature</th>
                            <th scope="col">1 hour average humidity</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.weatherData.map(w => {
                                i = i + 1;
                                return <WeatherTableTd
                                    id={i}
                                    weather={(w.weather)[0]}
                                    main={w.main}
                                />

                            })
                        }
                        </tbody>
                    </table>


                </div>
            </>
        )
    }
}

export default Home;