import React, {Component} from "react";

class WeatherTableTd extends Component {


    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <> <tr>
                <td>{this.props.id}</td>
                <td>{this.props.weather.description}</td>
                <td>{this.props.main.temp}</td>
                <td>{this.props.main.humidity}</td>
                <td>{this.props.main.avgTemp}</td>
                <td>{this.props.main.avgHumidity}</td>

            </tr>
            </>
        )
    }
}

export default WeatherTableTd;