import React, {Component} from "react";
import Link from "react-router-dom/es/Link";

class Header extends Component {


    constructor(props) {
        super(props);

    }

    componentDidMount() {
    }


    render() {
        return (

            <div>
                <nav className="navbar navbar-expand-lg    navbar-dark bg-dark">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">Open Weather Map</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Header;