import './App.css';
import React, {Suspense, lazy} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from "./components/Home"
import Header from "./components/Header";

function App() {
    return (
        <div className="App">

            <Router>
                <Header/>

                <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Route exact path="/" component={Home}/>

                    </Switch>
                </Suspense>
            </Router>

        </div>
    );
}

export default App;
